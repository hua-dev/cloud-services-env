var needle = require('needle');
var amqp = require('amqplib/callback_api');

function publishEvents(params) {

    console.log('[INFO] invoking HUA API /students/events');

    return new Promise(function (resolve, reject) {
        needle('get', `https://huademo.ddns.net/api/students/events?from=${params.from}&to=${params.to}`,
            {
                headers: {
                    'accept': 'application/json',
                    'authorization': `Bearer ${params.token}`
                }
            }
        )
            .then(res => {
                // console.log('-------START-OF-RESPONSE-------')
                // console.log(res.body);
                // console.log('--------END-OF-RESPONSE--------')

                if (res.body.status == 404) {
                    resolve({ events: [] });
                }

                console.log(`[INFO] found ${res.body.length} events`);

                let events = [];
                for (let i in res.body) {
                    let event = {
                        id: res.body[i].id,
                        createdDate: res.body[i].createdDate,
                        type: res.body[i].eventType,
                        user: res.body[i].student.id,
                        details: res.body[i].details
                    }
                    events.push(event);
                    sendToQueue('events', JSON.stringify(event));
                }

                resolve({ from: params.from, to: params.to, events: events.length});
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });
    });

}

const sendToQueue = (queue, msg) => {
    amqp.connect('amqp://huademo.ddns.net', function (error0, connection) {
        if (error0) {
            console.error(`[ERROR] unable to connect to amqp: ${error0}`);
            throw error0;
        }
        // console.log("[INFO] connected to amqp host");
        connection.createChannel(function (error1, channel) {
            if (error1) {
                console.error(`[ERROR] unable to create channel: ${error1}`);
                throw error1;
            }
            // console.log(`[INFO] created channel ${channel}`);

            channel.assertQueue(queue, {
                durable: false
            });
            channel.sendToQueue(queue, Buffer.from(msg));
            console.log(`[INFO] Sending ${msg}`);
        });
        setTimeout(function () {
            connection.close();
            process.exit(0);
        }, 500);
    });
};

exports.main = publishEvents;