var needle = require('needle');

function main(params) {

    console.log('invoking HUA API /students/events');
    
    return new Promise(function(resolve, reject) {
        needle('get', `https://huademo.ddns.net/api/students/events?from=${params.from}&to=${params.to}`, 
                {
                    headers: {
                        'accept': 'application/json',
                        'authorization': `Bearer ${params.token}`
                    }
                }
            )
            .then(res => {
                console.log('-------START-OF-RESPONSE-------')
                console.log(res.body);
                console.log('--------END-OF-RESPONSE--------')

                if (res.body.status == 404) {
                    resolve({events: []});
                }
                
                resolve({events: res.body});
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });
    });

}