#!/usr/bin/env node
var amqp = require('amqplib/callback_api');

function consumeEvents() {
    return new Promise(function (resolve, reject) {
        amqp.connect('amqp://huademo.ddns.net', function (error0, connection) {
            if (error0) {
                console.error(`[ERROR] unable to connect to amqp: ${error0}`);
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    console.error(`[ERROR] unable to create channel: ${error1}`);
                    throw error1;
                }

                const queue = 'events';
                let events = [];

                channel.assertQueue(queue, { durable: false });

                console.log(`[INFO] Waiting for messages in ${queue}`);

                channel.consume(queue, function (msg) {
                    console.log(`[INFO] Received ${msg.content.toString()}`);
                    const event = JSON.parse(msg.content.toString());
                    events.push(event);
                }, {
                    noAck: true
                });

                setTimeout(function () {
                    connection.close();
                    resolve({events: events});
                    // process.exit(0);
                }, 5000);
            });
        });
    });
}

exports.main = consumeEvents;