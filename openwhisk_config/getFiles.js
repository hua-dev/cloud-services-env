// var request = require('request');
var needle = require('needle');

function main(params) {

    console.log(`invoking HUA API /student/minio-files/${params.username}`);
    
    return new Promise(function(resolve, reject) {
        needle('get', `https://huademo.ddns.net/api/student/minio-files/${params.username}?from=${params.from}&to=${params.to}`, 
                {
                    headers: {
                        'accept': 'application/json',
                        'authorization': `Bearer ${params.token}`
                    }
                }
            )
            .then(res => {
                console.log('-------START-OF-RESPONSE-------')
                console.log(res.body);
                console.log('--------END-OF-RESPONSE--------')
                let files = [];
                for (const i in res.body) {
                    let file = res.body[i];
                    console.log(file);
                    files.push(file.fileName);
                }
                resolve({username: `${params.username}`, files: files});
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });
    });

}