#!/usr/bin/env node
var amqp = require('amqplib/callback_api');

function getEventsByType(params) {
    const queue = params.eventType;
    console.log(`[INFO] getting events with type=${queue}`);
    return new Promise(function (resolve, reject) {
        amqp.connect('amqp://huademo.ddns.net', function (error0, connection) {
            if (error0) {
                console.error(`[ERROR] unable to connect to amqp: ${error0}`);
                // throw error0;
                reject({error: error0});
            }
            console.log('[INFO] connected to amqp');
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    console.error(`[ERROR] unable to create channel: ${error1}`);
                    // throw error1;
                    reject({error: error1});
                }
                console.log('[INFO] channel created');

                // const queue = 'registrations';
                if (queue == undefined || !['registrations', 'password'].includes(queue)) {
                    console.error('[ERROR] param eventType is not set properly: {registrations|password}');
                    reject({error: 'event type is not properly set'});
                }
                let events = [];

                channel.assertQueue(queue, { durable: false });

                console.log(`[INFO] Waiting for messages in queue '${queue}'`);

                channel.consume(queue, function (msg) {
                    console.log(`[INFO] Received ${msg.content.toString()}`);
                    const event = JSON.parse(msg.content.toString());
                    events.push(event);
                }, {
                    noAck: true
                });

                setTimeout(function () {
                    connection.close();
                    resolve({events: events});
                    // process.exit(0);
                }, 5000);
            });
        });
    });
}

exports.main = getEventsByType;