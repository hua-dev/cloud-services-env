// var request = require('request');
var needle = require('needle');

function main(params) {

    console.log('invoking HUA API /auth/login');
    
    return new Promise(function(resolve, reject) {
        needle('post', 'https://huademo.ddns.net/api/auth/login', 
                {
                    username: params.username,
                    password: params.password
                }, 
                {json: true}
            )
            .then(res => {
                console.log(res.body);
                const body = res.body;
                resolve({name: body.name, id: body.id, token: body.token, roles: body.roles});
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });
    });

}