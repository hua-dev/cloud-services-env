FROM nodered/node-red:latest
RUN npm install node-red-contrib-minio-all
RUN npm install node-red-contrib-mp-function
RUN npm install node-red-node-openwhisk
RUN npm install @node-red-tools/node-red-contrib-amqp@2.0.1